package session1.flugplanung;

public class Flugplanung {

    private String flugnummer, flugdatum;
    private Klasse economyClass, businessClass;

    public Flugplanung(String flugnummer, String flugdatum, int economySitze, int buisnessSitze) {
        this.flugnummer = flugnummer;
        this.flugdatum = flugdatum;
        this.economyClass = new Klasse(economySitze);
        this.businessClass = new Klasse(buisnessSitze);
    }

    public boolean buchung(Passagier passagier, String klasse) {
        Passagier[] economyPassagiere = economyClass.getPassagiere();
        int economyFrei = 0;
        for (int i=0; i<economyPassagiere.length; i++) {
            if (economyPassagiere[i] != null) {
                if (economyPassagiere[i] == passagier)
                    return false;
            } else
                economyFrei++;
        }

        Passagier[] businessPassagiere = businessClass.getPassagiere();
        int businessFrei = 0;
        for (int i=0; i<businessPassagiere.length; i++) {
            if (businessPassagiere[i] != null) {
                if (businessPassagiere[i] == passagier)
                return false;
            } else
                businessFrei++;
        }

        if (klasse.toLowerCase().equals("economy")) {
            if (economyFrei == 0)
                return false;

            System.out.println(economyClass.addPassagier(passagier));
            return true;
        } else if (klasse.toLowerCase().equals("business")) {
            if (businessFrei == 0)
                return false;

            return businessClass.addPassagier(passagier);
        } else
            return false;
    }

    public void stornieren(String passnummer) {
        economyClass.removePassagier(passnummer);
        businessClass.removePassagier(passnummer);
    }

    public boolean upgrade(String passnummer, String klasse) {
        if (!klasse.equals("business")) // da ja ansonsten downgrade =/= Bezeichnung der Methode
            return false;

        Passagier[] businessPassagiere = businessClass.getPassagiere();
        for (Passagier passagier : businessClass.getPassagiere())
            if (passagier != null)
                if (passagier.getPassnummer().equals(passnummer)) {
                    this.stornieren(passnummer);
                    return this.buchung(passagier, "business");
                }

        return false;
    }

    @Override
    public String toString() {
        String output = "Flug " + flugnummer + " fliegt am " + flugdatum + " los. ";

        output += "\n" + "Economy Klasse: ";
        for (Passagier passagier : economyClass.getPassagiere())
            if (passagier != null)
                output += passagier.getName() + ", ";
        output = output.substring(0, output.length()-2);

        output += "\n" + "Business KLasse: ";
        for (Passagier passagier : businessClass.getPassagiere())
            if (passagier != null)
                output += passagier.getName() + ", ";
        output = output.substring(0, output.length()-2);

        output += "\n" + "Der Flug kann starten!";

        return output;
    }

}
