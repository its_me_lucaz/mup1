package session1.flugplanung;

public class Klasse {

    private Passagier[] passagiere;

    public Klasse(int maxPassagiere) {
        this.passagiere = new Passagier[maxPassagiere];
    }

    public Passagier[] getPassagiere() {
        return this.passagiere;
    }

    public Passagier getPassagier(String passnummer) {
        for (Passagier passagier : passagiere)
            if (passagier != null)
                if (passagier.getPassnummer().equals(passnummer))
                    return passagier;
        return null;
    }

    public boolean addPassagier(Passagier passagier) {
        for (int i=0; i<passagiere.length; i++) {
            if (passagiere[i] == null) {
                passagiere[i] = passagier;
                return true;
            }
        }
        return false;
    }

    public void removePassagier(String passnummer) {
        for (int i=0; i<passagiere.length; i++)
            if (passagiere[i] != null)
                if (passagiere[i].getPassnummer().equals(passnummer)) {
                    passagiere[i] = null;
                    return;
                }
    }

}
