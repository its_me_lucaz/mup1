package session1.flugplanung;

public class Passagier {

    private String passnummer, name;

    public Passagier(String passnummer, String name) {
        this.passnummer = passnummer;
        this.name = name;
    }

    public String getPassnummer() {
        return this.passnummer;
    }

    public String getName() {
        return this.name;
    }

}
