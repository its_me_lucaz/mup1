package session1;

import session1.flugplanung.Flugplanung;
import session1.flugplanung.Passagier;

public class Session1 {

    public static void main(String[] args) {
        Flugplanung flug = new Flugplanung("1", "09.11", 3, 4);

        Passagier a = new Passagier("1", "Herr A");
        Passagier b = new Passagier("2", "Frau B");
        Passagier c = new Passagier("3", "Kind C");
        Passagier d = new Passagier("4", "Bellgadse D");
        Passagier e = new Passagier("5", "Gadse E");

        flug.buchung(a, "business");
        flug.buchung(b, "business");
        flug.buchung(c, "business");
        flug.buchung(d, "economy");
        flug.buchung(e, "economy");

        System.out.println(flug.toString());

        flug.upgrade("2", "business");

        System.out.println(flug.toString());
    }

}
