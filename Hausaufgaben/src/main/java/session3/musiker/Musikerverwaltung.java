package session3.musiker;

import session3.musiker.data.Musikerdaten;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Musikerverwaltung {

    private List<Musiker> alleMusiker;

    public Musikerverwaltung() {
        this.alleMusiker = this.loadMusiker();
    }

    public LinkedList<Musiker> bubbleSort() {
        LinkedList<Musiker> data = new LinkedList<Musiker>(alleMusiker);

        for (int i=1; i<alleMusiker.size(); i++) {
            for (int j=0; j<alleMusiker.size()-1; j++) {
                // sort: geschlecht
                if (data.get(i).getGeschlecht() == 'w' && data.get(j).getGeschlecht() == 'm') {
                    Musiker man = data.get(i);
                    Musiker woman = data.get(j);

                    data.set(i, woman);
                    data.set(j, man);

                    continue;
                } else if (data.get(i).getGeschlecht() == 'm' && data.get(j).getGeschlecht() == 'w')
                    continue;

                // sort: dekade
                if (data.get(i).getDekade() < data.get(j).getDekade()) {
                    Musiker lower = data.get(i);
                    Musiker higher = data.get(j);

                    data.set(i, higher);
                    data.set(j, lower);

                    continue;
                } else if (data.get(i).getDekade() > data.get(j).getDekade())
                    continue;

                // sort: wirkungsorte
                if (data.get(i).getWirkungsOrte().size() < data.get(j).getWirkungsOrte().size()) {
                    Musiker fewer = data.get(i);
                    Musiker more = data.get(j);

                    data.set(i, more);
                    data.set(j, fewer);

                    continue;
                } else if (data.get(i).getWirkungsOrte().size() < data.get(j).getWirkungsOrte().size())
                    continue;

                // more
            }
        }

        return data;
    }

    public List<Musiker> filter(List<Musiker> musiker, char geschlecht) {
        ArrayList<Musiker> data = new ArrayList<Musiker>();

        for (Musiker loop : musiker) {
            if (loop.getGeschlecht() == geschlecht)
                data.add(loop);
        }

        return data;
    }

    public List<Musiker> filter(List<Musiker> musiker, String beruf) {
        ArrayList<Musiker> data = new ArrayList<Musiker>();

        for (Musiker loop : musiker) {
            if (loop.getMusikerBerufe().contains(beruf) || loop.getNormaleBerufe().contains(beruf))
                data.add(loop);
        }

        return data;
    }

    public List<Musiker> filter(List<Musiker> musiker, int von, int bis) {
        ArrayList<Musiker> data = new ArrayList<Musiker>();

        for (Musiker loop : musiker) {
            if (loop.getGeburtsjahr() >= von && loop.getGeburtsjahr() <= bis)
                data.add(loop);
        }

        return data;
    }

    public List<Musiker> filter(List<Musiker> musiker, List<String> orte) {
        ArrayList<Musiker> data = new ArrayList<Musiker>();

        for (Musiker loop : musiker) {
            if (loop.getWirkungsOrte().containsAll(orte))
                data.add(loop);
        }

        return data;
    }

    private List<Musiker> loadMusiker() {
        ArrayList<Musiker> data = new ArrayList<Musiker>();

        String[][] localData = Musikerdaten.getMusiker();
        for (String[] musiker : localData) {
            String name = musiker[0];
            char geschlecht = musiker[1].toCharArray()[0];
            int geburtsjahr = Integer.parseInt(musiker[2]);
            int sterbejahr = Integer.parseInt(musiker[3]);
            String[] musikerBerufe = musiker[4].split("#");
            String[] normaleBerufe = musiker[5].split("#");
            String[] wirkungsOrte = musiker[6].split("#");

            Musiker mensch = new Musiker(name, geschlecht, geburtsjahr, sterbejahr, musikerBerufe, normaleBerufe, wirkungsOrte);
            data.add(mensch);
        }

        return data;
    }

    public List<Musiker> getAlleMusiker() {
        return this.alleMusiker;
    }
}
