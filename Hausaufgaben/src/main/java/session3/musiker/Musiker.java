package session3.musiker;

import java.util.Arrays;
import java.util.List;

public class Musiker {

    private String name;
    private char geschlecht;
    private int geburtsjahr, sterbejahr;
    private List<String> musikerBerufe, normaleBerufe, wirkungsOrte;

    public Musiker(String name, char geschlecht, int geburtsjahr, int sterbejahr,
                   String[] musikerBerufe, String[] normaleBerufe, String[] wirkungsOrte) {
        this.name = name;
        this.geschlecht = geschlecht;
        this.geburtsjahr = geburtsjahr;
        this.sterbejahr = sterbejahr;
        this.musikerBerufe = Arrays.asList(musikerBerufe);
        this.normaleBerufe = Arrays.asList(normaleBerufe);
        this.wirkungsOrte = Arrays.asList(wirkungsOrte);
    }

    public int getDekade() {
       return ((geburtsjahr+9)/10)*10;
    }

    @Override
    public String toString() {
        return name + " (" + geschlecht + ")" + " geboren am " + geburtsjahr + " (t " + sterbejahr + ")" + " - Dekade: " + this.getDekade() + " - Wirkungsorte: " + wirkungsOrte.size();
    }

    public String getName() {
        return this.name;
    }

    public char getGeschlecht() {
        return this.geschlecht;
    }

    public int getGeburtsjahr() {
        return this.geburtsjahr;
    }

    public int getSterbejahr() {
        return this.sterbejahr;
    }

    public List<String> getMusikerBerufe() {
        return this.musikerBerufe;
    }

    public List<String> getNormaleBerufe() {
        return this.normaleBerufe;
    }

    public List<String> getWirkungsOrte() {
        return this.wirkungsOrte;
    }
}
