package session3.chiffre;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class Entschluessler {

    private String text;

    public Entschluessler(String text) {
        this.text = text;
    }

    public String entschluessle() {
        String answer = "";

        // 6
        for (String row : text.split("\n")) {
            for (String word : row.split(" ")) {
                StringBuilder builder = new StringBuilder();

                builder.append(word);
                builder.reverse();

                answer += builder.toString() + " ";
            }
        }

        // 5
        answer = answer.replaceAll(" k", "k").replaceAll("l\no", "lo");

        // 4 & 3
        List<String> sqrtNumbers = new LinkedList<String>();
        Map<Integer, Character> fqValues = new LinkedHashMap<Integer, Character>();
        String currentNumber = "";
        for (int i=0; i<answer.length(); i++) {
            char letter = answer.charAt(i);

            switch (letter) {
                case '0':
                case '1':
                case '2':
                case '3':
                case '4':
                case '5':
                case '6':
                case '7':
                case '8':
                case '9':
                    currentNumber += letter;
                    break;
                default:
                    if (!currentNumber.equals("")) {
                        sqrtNumbers.add(currentNumber);
                        currentNumber = "";
                    }

                    if (letter == 'f')
                        fqValues.put(i, 'q');
                    else if (letter == 'q')
                        fqValues.put(i, 'f');
                    break;
            }
        }

        // 3
        StringBuilder fqBuilder = new StringBuilder(answer);
        for (Map.Entry<Integer, Character> entry : fqValues.entrySet()) {
            fqBuilder.setCharAt(entry.getKey(), entry.getValue());
        }
        answer = fqBuilder.toString();

        // 4
        for (String number : sqrtNumbers) {
            int value = Integer.parseInt(number);
            answer = answer.replaceAll("" + value, "" + Double.valueOf(Math.sqrt(value)).intValue());
        }

        // 2
        String move = "";
        for (int i=0; i<answer.length(); i++) {
            char letter = answer.charAt(i);
            int ascii = (int) letter;

            move += Character.toString((char) (ascii + 3));
        }
        answer = move;

        // 1
        answer = answer.replaceAll(":", "\n").replaceAll("#", " ");

        return answer.toString();
    }

}
