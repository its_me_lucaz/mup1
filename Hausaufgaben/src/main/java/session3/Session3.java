package session3;


import session3.chiffre.Chiffre;
import session3.chiffre.Entschluessler;
import session3.musiker.Musiker;
import session3.musiker.Musikerverwaltung;

import java.util.Arrays;
import java.util.List;

public class Session3 {

    public static void main(String... args) {
        // MUSIKER
        Musikerverwaltung verwaltung = new Musikerverwaltung();

        List<Musiker> geboreneMusiker = verwaltung.filter(verwaltung.getAlleMusiker(), 1801, 1850);
        List<Musiker> pianisten = verwaltung.filter(geboreneMusiker, "Pianist");
        List<Musiker> lokaleMusiker = verwaltung.filter(pianisten, Arrays.asList("München", "Salzburg"));

        for (Musiker gefilterte : verwaltung.bubbleSort())
            System.out.println(gefilterte);

        // CHIFFRE
        Entschluessler entschluessler = new Entschluessler(Chiffre.getText());
        System.out.println(entschluessler.entschluessle());
    }

}
