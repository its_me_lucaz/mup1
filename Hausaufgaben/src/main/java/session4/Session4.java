package session4;

import session4.spielfeld.Gitter;
import session4.spielfeld.Gitter3D;

public class Session4 {

    public static void main(String... args) {
        Gitter gitter = new Gitter(10);

        gitter.getStartUndZiel();
        gitter.findeZiel();

        System.out.println(gitter);
    }

}
