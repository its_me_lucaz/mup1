package session4.spielfeld;

import lombok.Getter;

public class Feld3D extends Feld {

    @Getter
    private final int tiefe;

    public Feld3D(int zeile, int spalte, int tiefe) {
        super(zeile, spalte);
        this.tiefe = tiefe;
    }

    @Override
    public String toString() {
        return "3DFeld " + super.getZeile() + "-" + super.getSpalte() + "-" + this.getTiefe() + "(" + this.isTrap() + ")";
    }
}
