package session4.spielfeld;

public interface Traceable {

    void getStartUndZiel();
    void findeZiel();
    void findeRand();

}
