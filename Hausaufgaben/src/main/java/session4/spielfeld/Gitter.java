package session4.spielfeld;

import java.util.*;

public class Gitter implements Traceable {

    private final int groesse;
    private final Map<String, Feld> felder;

    private final Stack<Feld> zielPfad;
    private final Stack<Feld> randPfad;
    private final List<Feld> sackgasse;

    private int tries;

    private Feld start, ziel;

    public Gitter(int groesse) {
        this.groesse = groesse;
        this.tries = groesse * groesse;

        this.felder = new HashMap<String, Feld>();
        this.zielPfad = new Stack<Feld>();
        this.randPfad = new Stack<Feld>();
        this.sackgasse = new ArrayList<Feld>();

        for (int zeile=1; zeile<=groesse; zeile++)
            for (int spalte=1; spalte<=groesse; spalte++)
                felder.put(zeile + "-" + spalte, new Feld(zeile, spalte));
    }

    public void getStartUndZiel() {
        while (start == null) {
            start = this.getRandomFeld();

            if (start == null)
                continue;

            if (start.isTrap())
                start = null;
        }

        while (ziel == null) {
            ziel = this.getRandomFeld();

            if (ziel == null)
                continue;

            if (ziel.isTrap()) {
                ziel = null;
                continue;
            }

            if (distance(start, ziel) <= 2) {
                ziel = null;
                continue;
            }
        }
    }

    public void findeZiel() {
        if (start == null || ziel == null)
            return;

        Feld aktuell;
        if (zielPfad.size() == 0)
            aktuell = start;
        else
            aktuell = zielPfad.peek();

        Feld[] angrenzendeFelder = this.getAngrenzendeFelder(aktuell);
        List<Feld> avaiable = new ArrayList<Feld>();
        for (Feld loop : angrenzendeFelder) {
            if (loop != null && !loop.isTrap() && !zielPfad.contains(loop) && !sackgasse.contains(loop) && loop != start)
                avaiable.add(loop);
        }

        if (avaiable.contains(ziel))
            return;

        Random random = new Random();
        if (avaiable.size() == 0) {
            if (zielPfad.size() > 0) {
                sackgasse.add(aktuell);
                zielPfad.pop();
            } else
                return;
        } else
            zielPfad.push(avaiable.get(random.nextInt(avaiable.size())));

        if (tries == 0)
            return;

        tries--;
        this.findeZiel();
    }

    public void findeRand() {
        if (start == null)
            return;

        Feld aktuell;
        if (randPfad.size() == 0)
            aktuell = start;
        else
            aktuell = randPfad.peek();

        Feld[] angrenzendeFelder = this.getAngrenzendeFelder(aktuell);
        List<Feld> avaiable = new ArrayList<Feld>();
        for (Feld loop : angrenzendeFelder) {
            if (loop == null)
                return;
            else if (!loop.isTrap() && !randPfad.contains(loop) && !sackgasse.contains(loop) && loop != start && loop != ziel)
                avaiable.add(loop);
        }

        Random random = new Random();
        if (avaiable.size() == 0) {
            if (randPfad.size() > 0) {
                sackgasse.add(aktuell);
                randPfad.pop();
            } else
                return;
        } else
            randPfad.push(avaiable.get(random.nextInt(avaiable.size())));

        if (tries == 0)
            return;

        tries--;
        this.findeRand();
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();

        for (int zeile=1; zeile<=groesse; zeile++) {
            for (int spalte = 1; spalte <= groesse; spalte++) {
                Feld feld = felder.get(zeile + "-" + spalte);

                if (feld == start)
                    builder.append("S");
                else if (feld == ziel)
                    builder.append("E");
                else if (feld.isTrap())
                    builder.append("#");
                else if (zielPfad != null && zielPfad.contains(feld))
                    builder.append("x");
                else if (randPfad != null && randPfad.contains(feld))
                    builder.append("r");
                else
                    builder.append("O");

                builder.append(" ");
            }
            builder.append("\n");
        }

        return builder.toString();
    }

    private Feld[] getAngrenzendeFelder(Feld aktuell) {
        Random random = new Random();

        int zeile = aktuell.getZeile();
        int spalte = aktuell.getSpalte();

        Feld[] angrenzendeFelder = new Feld[4];
        angrenzendeFelder[0] = felder.get((zeile-1) + "-" + spalte);
        angrenzendeFelder[1] = felder.get(zeile + "-" + (spalte+1));
        angrenzendeFelder[2] = felder.get((zeile+1) + "-" + spalte);
        angrenzendeFelder[3] = felder.get(zeile + "-" + (spalte-1));

        return angrenzendeFelder;
    }

    private Feld getRandomFeld() {
        Random random = new Random();
        return felder.get(random.nextInt(groesse+1) + "-" + random.nextInt(groesse+1));
    }

    private int distance(Feld feld1, Feld feld2) {
        return Math.abs(feld1.getZeile()-feld2.getZeile()) + Math.abs(feld1.getSpalte()-feld2.getSpalte());
    }

}
