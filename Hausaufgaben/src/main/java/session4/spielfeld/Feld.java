package session4.spielfeld;

import lombok.Getter;

import java.util.Random;

public class Feld {

    @Getter
    private final int zeile, spalte;
    private final int[] zahlen;

    Feld(int zeile, int spalte) {
        this.zeile = zeile;
        this.spalte = spalte;

        this.zahlen = new int[3];

        Random random = new Random();
        for (int i=0; i<zahlen.length; i++)
            this.zahlen[i] = random.nextInt(900) + 100;
    }

    public boolean isTrap() {
        for (int i=0; i<zahlen.length; i++)
            if (isPrimzahl(zahlen[i]))
                return true;
        return false;
    }

    @Override
    public String toString() {
        return "Feld " + zeile + "-" + spalte + "(" + this.isTrap() + ")";
    }

    private boolean isPrimzahl(int value) {
        if (value <= 2)
            return (value == 2);

        for (long i = 2; i*i <= value; i++)
            if (value % i == 0)
                return false;

        return true;
    }

}
