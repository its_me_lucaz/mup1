package session4.spielfeld;

import java.util.*;

public class Gitter3D implements Traceable {

    private final int groesse;
    private final Map<String, Feld3D> felder;

    private final Stack<Feld3D> zielPfad;
    private final Stack<Feld3D> randPfad;
    private final List<Feld3D> sackgasse;

    private int tries;

    private Feld3D start, ziel;

    public Gitter3D(int groesse) {
        this.groesse = groesse;
        this.tries = groesse * groesse * groesse;

        this.felder = new HashMap<String, Feld3D>();
        this.zielPfad = new Stack<Feld3D>();
        this.randPfad = new Stack<Feld3D>();
        this.sackgasse = new ArrayList<Feld3D>();

        for (int zeile=1; zeile<=groesse; zeile++)
            for (int spalte=1; spalte<=groesse; spalte++)
                for (int tiefe=1; tiefe<=groesse; tiefe++)
                    felder.put(zeile + "-" + spalte + "-" + tiefe, new Feld3D(zeile, spalte, tiefe));
    }

    public void getStartUndZiel() {
        while (start == null) {
            start = this.getRandomFeld();

            if (start == null)
                continue;

            if (start.isTrap())
                start = null;
        }

        while (ziel == null) {
            ziel = this.getRandomFeld();

            if (ziel == null)
                continue;

            if (ziel.isTrap()) {
                ziel = null;
                continue;
            }

            if (distance(start, ziel) <= 2) {
                ziel = null;
                continue;
            }
        }
    }

    public void findeZiel() {
        if (start == null || ziel == null)
            return;

        Feld3D aktuell;
        if (zielPfad.size() == 0)
            aktuell = start;
        else
            aktuell = zielPfad.peek();

        Feld3D[] angrenzendeFelder = this.getAngrenzendeFelder(aktuell);
        List<Feld3D> avaiable = new ArrayList<Feld3D>();
        for (Feld3D loop : angrenzendeFelder) {
            if (loop != null && !loop.isTrap() && !zielPfad.contains(loop) && !sackgasse.contains(loop) && loop != start)
                avaiable.add(loop);
        }

        if (avaiable.contains(ziel))
            return;

        Random random = new Random();
        if (avaiable.size() == 0) {
            if (zielPfad.size() > 0) {
                sackgasse.add(aktuell);
                zielPfad.pop();
            } else
                return;
        } else
            zielPfad.push(avaiable.get(random.nextInt(avaiable.size())));

        if (tries == 0)
            return;

        tries--;
        this.findeZiel();
    }

    public void findeRand() {
        if (start == null)
            return;

        Feld3D aktuell;
        if (randPfad.size() == 0)
            aktuell = start;
        else
            aktuell = randPfad.peek();

        Feld3D[] angrenzendeFelder = this.getAngrenzendeFelder(aktuell);
        List<Feld3D> avaiable = new ArrayList<Feld3D>();
        for (Feld3D loop : angrenzendeFelder) {
            if (loop == null)
                return;
            else if (!loop.isTrap() && !randPfad.contains(loop) && !sackgasse.contains(loop) && loop != start && loop != ziel)
                avaiable.add(loop);
        }

        Random random = new Random();
        if (avaiable.size() == 0) {
            if (randPfad.size() > 0) {
                sackgasse.add(aktuell);
                randPfad.pop();
            } else
                return;
        } else
            randPfad.push(avaiable.get(random.nextInt(avaiable.size())));

        if (tries == 0)
            return;

        tries--;
        this.findeRand();
    }

    private Feld3D[] getAngrenzendeFelder(Feld3D aktuell) {
        Random random = new Random();

        int zeile = aktuell.getZeile();
        int spalte = aktuell.getSpalte();
        int tiefe = aktuell.getTiefe();

        Feld3D[] angrenzendeFelder = new Feld3D[6];
        angrenzendeFelder[0] = felder.get((zeile-1) + "-" + spalte + "-" + tiefe);
        angrenzendeFelder[1] = felder.get((zeile+1) + "-" + spalte + "-" + tiefe);
        angrenzendeFelder[2] = felder.get(zeile + "-" + (spalte-1) + "-" + tiefe);
        angrenzendeFelder[3] = felder.get(zeile + "-" + (spalte+1) + "-" + tiefe);
        angrenzendeFelder[4] = felder.get(zeile + "-" + spalte  + "-" + (tiefe-1));
        angrenzendeFelder[5] = felder.get(zeile + "-" + spalte  + "-" + (tiefe+1));

        return angrenzendeFelder;
    }

    private Feld3D getRandomFeld() {
        Random random = new Random();
        return felder.get(random.nextInt(groesse+1) + "-" + random.nextInt(groesse+1) + "-" + random.nextInt(groesse+1));
    }

    private int distance(Feld3D feld1, Feld3D feld2) {
        return Math.abs(feld1.getZeile()-feld2.getZeile()) + Math.abs(feld1.getSpalte()-feld2.getSpalte()) + Math.abs(feld1.getTiefe()-feld2.getTiefe());
    }

}
