package session2.verwaltung;

import session2.naturkatrastophe.Naturkatastrophe;

import java.util.Comparator;

public class DatumSortierer implements Comparator<Naturkatastrophe> {

    public int compare(Naturkatastrophe o1, Naturkatastrophe o2) {
        return o1.getDatum().compareTo(o2.getDatum());
    }

}
