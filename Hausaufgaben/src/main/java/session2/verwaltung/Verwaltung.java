package session2.verwaltung;

import session2.database.NaturkatastrophenDB;
import session2.naturkatrastophe.Erdbeben;
import session2.naturkatrastophe.Naturkatastrophe;
import session2.naturkatrastophe.Tsunami;
import session2.naturkatrastophe.Vulkanausbruch;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class Verwaltung {

    private final List<Naturkatastrophe> naturkatastrophen;

    public Verwaltung() {
        this.naturkatastrophen = this.getNaturkatastrophenAsList();
    }

    public void printSchwereNaturkatastrophen() {
        System.out.println("Schwere Naturkatastrophen:" + "\n");

        for (Naturkatastrophe katastrophe : naturkatastrophen) {
            if (katastrophe.getOpfer() <= 0)
                continue;

            if (katastrophe instanceof Vulkanausbruch) {
                Vulkanausbruch vulkan = (Vulkanausbruch) katastrophe;
                if (vulkan.getIndex() >= 3)
                    System.out.println(vulkan);
                else
                    continue;
            } else if (katastrophe instanceof Erdbeben) {
                Erdbeben erdbeben = (Erdbeben) katastrophe;
                if (erdbeben.getSkala() >= 7.0)
                    System.out.println(erdbeben);
                else
                    continue;
            } else if (katastrophe instanceof Tsunami) {
                Tsunami tsunami = (Tsunami) katastrophe;
                if (tsunami.getWasserhoehe() >= 1.0)
                    System.out.println(tsunami);
                else
                    continue;
            } else
                continue;

            System.out.println("=======================================");
        }
    }

    public void printSortierteNaturkatastrophen() {
        List<Naturkatastrophe> sortierung = new ArrayList<Naturkatastrophe>(naturkatastrophen);
        Collections.sort(sortierung, new DatumSortierer());

        System.out.println("Sortierung nach Datum:" + "\n");

        for (Naturkatastrophe naturkatastrophe : sortierung) {
            if (naturkatastrophe instanceof Vulkanausbruch)
                System.out.println((Vulkanausbruch) naturkatastrophe);
            else if (naturkatastrophe instanceof Erdbeben)
                System.out.println((Erdbeben) naturkatastrophe);
            else if (naturkatastrophe instanceof Tsunami)
                System.out.println(naturkatastrophe);
            else
                continue;

            System.out.println("=======================================");
        }
    }

    public void printGemeinsameKatastrophen(Naturkatastrophe katastrophe) {
        Date datum = katastrophe.getDatum();

        System.out.println("Gemeinsame Naturkatastrophen von" + "\n" + katastrophe + "\n");

        for (Naturkatastrophe loop : naturkatastrophen) {
            if (!loop.getDatum().equals(datum))
                continue;

            if (this.getDistance(katastrophe, loop) < 100)
                System.out.println(loop);
        }
    }

    public void printMatrikelnummerAbhaenigeKatastrophe() {
        System.out.println("Erdbeben im November 2015:" + "\n");

        for (Naturkatastrophe naturkatastrophe : naturkatastrophen) {
            if (naturkatastrophe instanceof Erdbeben) {
                Erdbeben erdbeben = (Erdbeben) naturkatastrophe;

                if (erdbeben.getDatum().getMonth() == 10 && (erdbeben.getDatum().getYear()+1900) == 2015) {
                    System.out.println(erdbeben);
                    System.out.println("=======================================");
                }
            }
        }
    }

    private double getDistance(Naturkatastrophe kat1, Naturkatastrophe kat2) {
        double eineVariable = Math.sin(kat1.getBreitengrad()) * Math.sin(kat2.getBreitengrad()) +
                              Math.cos(kat1.getBreitengrad() * Math.cos(kat2.getBreitengrad() *
                              Math.cos(kat1.getLaengengrad() - kat2.getLaengengrad())));

        return 6378 * Math.acos(eineVariable);
    }

    private final List<Naturkatastrophe> getNaturkatastrophenAsList() {
        List<Naturkatastrophe> naturkatrastophen = new ArrayList<Naturkatastrophe>();

        String[][] data = NaturkatastrophenDB.getEvents();
        for (String[] entry : data) {
            String typ = entry[0];
            Date datum = new Date(Integer.valueOf(entry[1]) - 1900,
                                  Integer.valueOf(entry[2]) - 1,
                                  Integer.valueOf(entry[3]));
            double breitengrad = Double.valueOf(entry[4]);
            double laengengrad = Double.valueOf(entry[5]);
            String ort = entry[6];
            int opfer = Integer.valueOf(entry[7]);

            if (typ.equalsIgnoreCase("V")) {
                String vulkanArt = entry[8];
                int meeresspiegel = Integer.valueOf(entry[9]);
                int index = Integer.valueOf(entry[10]);

                Vulkanausbruch vulkan = new Vulkanausbruch(datum, laengengrad, breitengrad, ort, opfer,
                        vulkanArt, meeresspiegel, index);

                naturkatrastophen.add(vulkan);
            } else if (typ.equalsIgnoreCase("E")) {
                double skala = Double.valueOf(entry[8]);
                int ausbreitung = Integer.valueOf(entry[9]);

                Erdbeben erdbeben = new Erdbeben(datum, laengengrad, breitengrad, ort, opfer,
                        skala, ausbreitung);

                naturkatrastophen.add(erdbeben);
            } else if (typ.equalsIgnoreCase("T")) {
                int ursache = Integer.valueOf(entry[8]);
                double wasserhoehe = Double.valueOf(entry[9]);

                Tsunami tsunami = new Tsunami(datum, laengengrad, breitengrad, ort, opfer,
                        ursache, wasserhoehe);

                naturkatrastophen.add(tsunami);
            }
        }

        return naturkatrastophen;
    }

}
