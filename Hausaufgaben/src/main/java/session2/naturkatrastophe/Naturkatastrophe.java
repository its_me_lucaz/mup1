package session2.naturkatrastophe;

import java.text.SimpleDateFormat;
import java.util.Date;

public abstract class Naturkatastrophe {

    private final SimpleDateFormat format;

    private Date datum;
    private double laengengrad, breitengrad;
    private String ort;
    private int opfer;

    public Naturkatastrophe(Date datum, double laengengrad, double breitengrad, String ort, int opfer) {
        this.datum = datum;
        this.laengengrad = laengengrad;
        this.breitengrad = breitengrad;
        this.ort = ort;
        this.opfer = opfer;

        this.format = new SimpleDateFormat("dd.MM.yyyy");
    }

    public abstract String toString();

    public SimpleDateFormat getFormat() {
        return this.format;
    }

    public Date getDatum() {
        return this.datum;
    }

    public double getLaengengrad() {
        return this.laengengrad;
    }

    public double getBreitengrad() {
        return this.breitengrad;
    }

    public String getOrt() {
        return this.ort;
    }

    public int getOpfer() {
        return this.opfer;
    }
}
