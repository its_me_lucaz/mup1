package session2.naturkatrastophe;

import java.util.Date;

public class Erdbeben extends Naturkatastrophe {

    private double skala;
    private int breite;

    public Erdbeben(Date datum, double laengengrad, double breitengrad, String ort, int opfer,
                    double skala, int breite) {
        super(datum, laengengrad, breitengrad, ort, opfer);

        this.skala = skala;
        this.breite = breite;
    }

    @Override
    public String toString() {
        String schoenesDatum = this.getFormat().format(this.getDatum());

        return "Erdbeben am " + schoenesDatum + " in " + this.getOrt() + "[" + this.getLaengengrad() + " / " + this.getBreitengrad() + "]"
                + "\n"
                + "Opfer: " + this.getOpfer()
                + "\n"
                + "Skala: " + this.getSkala()
                + "\n"
                + "Ausbreitung: " + this.getBreite();
    }

    public double getSkala() {
        return this.skala;
    }

    public int getBreite() {
        return this.breite;
    }
}
