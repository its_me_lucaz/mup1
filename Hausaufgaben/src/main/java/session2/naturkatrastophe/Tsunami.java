package session2.naturkatrastophe;

import java.util.Date;

public class Tsunami extends Naturkatastrophe {

    private int ursache;
    private double wasserhoehe;

    public Tsunami(Date datum, double laengengrad, double breitengrad, String ort, int opfer,
                   int ursache, double wasserhoehe) {
        super(datum, laengengrad, breitengrad, ort, opfer);

        this.ursache = ursache;
        this.wasserhoehe = wasserhoehe;
    }

    @Override
    public String toString() {
        String schoenesDatum = this.getFormat().format(this.getDatum());

        return "Tsunami am " + schoenesDatum + " in " + this.getOrt() + "[" + this.getLaengengrad() + " / " + this.getBreitengrad() + "]"
                + "\n"
                + "Opfer: " + this.getOpfer()
                + "\n"
                + "Ursache: " + this.getUrsache()
                + "\n"
                + "Wasserhöhe: " + this.getWasserhoehe();
    }

    public int getUrsache() {
        return this.ursache;
    }

    public double getWasserhoehe() {
        return this.wasserhoehe;
    }
}
