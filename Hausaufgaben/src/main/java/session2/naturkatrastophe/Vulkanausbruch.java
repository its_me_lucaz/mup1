package session2.naturkatrastophe;

import java.util.Date;

public class Vulkanausbruch extends Naturkatastrophe {

    private String typ;
    private int meeresspiegel;
    private int index;

    public Vulkanausbruch(Date datum, double laengengrad, double breitengrad, String ort, int opfer,
                          String typ, int meeresspiegel, int index) {
        super(datum, laengengrad, breitengrad, ort, opfer);

        this.typ = typ;
        this.meeresspiegel = meeresspiegel;
        this.index = index;
    }

    @Override
    public String toString() {
        String schoenesDatum = this.getFormat().format(this.getDatum());

        return "Vulkanausbruch am " + schoenesDatum + " in " + this.getOrt() + "[" + this.getLaengengrad() + " / " + this.getBreitengrad() + "]"
                + "\n"
                + "Opfer: " + this.getOpfer()
                + "\n"
                + "Vulkan-Art: " + this.getTyp()
                + "\n"
                + "Meeresspiegel: " + this.getMeeresspiegel()
                + "\n"
                + "Index: " + this.getIndex();
    }

    public String getTyp() {
        return this.typ;
    }

    public int getMeeresspiegel() {
        return this.meeresspiegel;
    }

    public int getIndex() {
        return this.index;
    }
}
