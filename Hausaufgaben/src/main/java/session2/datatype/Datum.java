package session2.datatype;

public class Datum {

    private int jahr, monat, tag;

    public Datum(int tag, int monat, int jahr) {
        this.tag = tag;
        this.monat = monat;
        this.jahr = jahr;
    }

    @Override
    public String toString() {
        return tag + "." + monat + "." + jahr;
    }

}
