package part1;

public class Main {

    public static void main(String[] args) {
        Person angelaMerkel = new Person(165, 'w', "Angela Merkel", new Datum(17, 7, 1954));
        Person martinSchulz = new Person(185, 'm', "Martin Schulz", new Datum(20, 12, 1955));

        System.out.println("Hallo mein Name ist " + angelaMerkel.getName() + ". Geburtstag: " + angelaMerkel.getDatum().getDatum());

    }

}
