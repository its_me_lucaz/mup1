package part1;

public class Person {

    private int groesse;
    private char geschlecht;
    private String name;
    private Datum datum;

    public Person(int groesse, char geschlecht, String name, Datum datum) {
        this.groesse = groesse;
        this.geschlecht = geschlecht;
        this.name = name;
        this.datum = datum;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Datum getDatum() {
        return datum;
    }

}
